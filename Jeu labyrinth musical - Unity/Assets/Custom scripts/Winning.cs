﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ItemToFind
{
    public string name;
    public GameObject triggerZone;
    public bool found;
    
}

public class Winning : MonoBehaviour {

    public GameObject WinningText;
    public Text textItemFound;
    public List<ItemToFind> toFind;
	// Use this for initialization
	void Start () {
        WinningText.SetActive(false);
        CheckWinState();
	}
	// Update is called once per frame
	void Update () {
		
	}

    public void ItemFound(GameObject key) {
        Debug.Log("ItemFound called");
        if (SetValueOf(key, true)) {
            textItemFound.text += "\n" + GetValueOf(key).name;
            CheckWinState();
        }
    }
    public bool SetValueOf(GameObject key, bool value) {
        Debug.Log("trying to find object " + key);
        for (int i = 0; i < toFind.Count; i++) 
            if (GameObject.ReferenceEquals(toFind[i].triggerZone, key)) {
                if (toFind[i].found == false) {
                    toFind[i].found = value;
                    return true;
                }
                return false;
            }
        return false;
    }

    public ItemToFind GetValueOf(GameObject key) {
        for (int i = 0; i < toFind.Count; i++)
            if (GameObject.ReferenceEquals(toFind[i].triggerZone, key)) {
                return toFind[i];
            }
        return null;
    }

    public void CheckWinState() {
        if (GameWon()) {
            WinningText.SetActive(true);
        }
    }

    public bool GameWon() {
        foreach(var item in toFind) 
            if (item.found == false)
                return false;
        return true;
    }
}
