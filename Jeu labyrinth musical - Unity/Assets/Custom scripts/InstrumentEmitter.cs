﻿using System;
using UnityEngine;


public class InstrumentEmitter : BrouillardBehaviour
{

    [FMODUnity.EventRef]
    public string instrument;
    FMOD.Studio.EventInstance soundEvent;

    public GameObject SoundCollider;
    //public GameObject Hammer;


    float MAX_IMPULSE = 80;
    Rigidbody myRigidbody;
    Collider soundCollider;
    //Collider hammerCollider;
    //Rigidbody hammerRigidbod;




    // Use this for initialization
    void Start() {
        //Debug.Log("??");
        myRigidbody = GetComponent<Rigidbody>();
        //Debug.Log("myRigidbody : " + myRigidbody);
        soundCollider = SoundCollider.transform.GetComponent<Collider>();
        //Debug.Log("tambour : " + soundCollider + " from " + this);
        //hammerCollider = Hammer.GetComponent<Collider>();
        //Debug.Log("hammer : " + Hammer + " from " + this);
        //hammerRigidbod = Hammer.GetComponent<Rigidbody>();
        //Debug.Log("hammer : " + hammerRigidbod + " from " + this);
        soundEvent = FMODUnity.RuntimeManager.CreateInstance(instrument);
        //Debug.Log("soundEvent : " + soundEvent + " from " + this);

    }

    void OnCollisionEnter(Collision col) {
        //if there was a collision with the soundCollider
        if (Collider.ReferenceEquals(col.contacts[0].thisCollider, soundCollider)) {
            soundEvent = FMODUnity.RuntimeManager.CreateInstance(instrument);
            //Debug.Log("collision between " + col.contacts[0].thisCollider + " and " + col.collider + "with impulse : " + col.impulse.magnitude);
            //Debug.Log("relative velocity is : " + col.relativeVelocity);

            float powerValue = Math.Min(col.relativeVelocity.magnitude, MAX_IMPULSE);
            soundEvent.setParameterValue("POWER", powerValue);
            soundEvent.setParameterValue("Brouillard", brouillard);
            soundEvent.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));

            //Debug.Log("Power value is " + powerValue);
            soundEvent.start();
            soundEvent.release();

        }
    }


    // Update is called once per frame
    void Update() {
        soundEvent.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject, myRigidbody));
    }


}
