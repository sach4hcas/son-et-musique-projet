﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleMusic : MonoBehaviour {

	[FMODUnity.EventRef]
    public string title;
    FMOD.Studio.EventInstance titleEvent;

    Rigidbody myRigidbody;

    // Use this for initialization
    void Start () {
        titleEvent = FMODUnity.RuntimeManager.CreateInstance(title);
		titleEvent.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        titleEvent.start();
    }
}
