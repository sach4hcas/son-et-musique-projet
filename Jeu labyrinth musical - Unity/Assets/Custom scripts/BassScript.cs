﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BassScript : StyleBehaviour {

    [FMODUnity.EventRef]
    public string bass;
    FMOD.Studio.EventInstance bassEvent;

    Rigidbody myRigidbody;

    bool started = false;

    // Use this for initialization
    void Start () {
        bassEvent= FMODUnity.RuntimeManager.CreateInstance(bass);
        myRigidbody = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
        bassEvent.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject, myRigidbody));
        bassEvent.setParameterValue("Brouillard", brouillard);
        bassEvent.setParameterValue("style", style);
        if (!started && Time.timeSinceLevelLoad > 3.1) {
            Debug.Log("starting bass");
            started = true;
            bassEvent.start();
        }

    }
    
}
