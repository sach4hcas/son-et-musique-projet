﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StyleBehaviour : BrouillardBehaviour {

    int numberOfStyle = 2;
    protected float style = 0.5f;

    public void ChangeStyle() {
        
        style = (style + 1) % numberOfStyle;
        Debug.Log("changing style on " + this + ", style is now : " + style);
    }
}
