﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

[System.Serializable]
public enum NoteLead
{
    doubleCroche = 16,
    croche = 8,
    noir = 4,
    blanche = 2,
    ronde = 1
}

public class LeadScript : StyleBehaviour
{

    [FMODUnity.EventRef]
    public string lead;
    FMOD.Studio.EventInstance leadEvent;

    [SerializeField]
    public int tempo = 120;
    [SerializeField]
    public int nbNote = 4;
    [SerializeField]
    public NoteLead note = NoteLead.noir;

    //public Bool2D mesures = new Bool2D();
    [SerializeField]
    public List<BoolIntArray> mesures;


    float startingTime;
    float tempsUntilNextNote;

    int mesuresIndex = 0;
    int notesIndex = 0;
    int mesureCount = 0;
    float styleLead;

    float[] pitches;
    int currentIndex;

    bool started = false;

    Rigidbody myRigidbody;



    // Use this for initialization
    void Start() {
        styleLead = style;
        pitches = new float[15] { 0.5F, 0.561F, 0.630F, 0.667F, 0.749F, 0.841F, 0.944F, 1F, 1.122F, 1.260F, 1.335F, 1.498F, 1.681F, 1.888F, 2F };
        currentIndex = 7;

        myRigidbody = GetComponent<Rigidbody>();
        leadEvent = FMODUnity.RuntimeManager.CreateInstance(lead);


    }

    void PlaySound(float volume) {

        leadEvent = FMODUnity.RuntimeManager.CreateInstance(lead);
        leadEvent.setParameterValue("pitch", getNextPitch());
        leadEvent.setParameterValue("POWER", volume);
        leadEvent.setParameterValue("Brouillard", brouillard);
        leadEvent.setParameterValue("style", styleLead);
        leadEvent.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        leadEvent.start();
        leadEvent.release();

    }


    void HandleFirstNote() {
        started = true;
        if (mesures[0][0].isTrue)
            PlaySound(mesures[0][0].integer);
        notesIndex = (notesIndex + 1) % mesures[mesuresIndex].Count();
        if (notesIndex == 0)
            mesuresIndex = (mesuresIndex + 1) % mesures.Count;
        startingTime = Time.time;
        float coeff = mesures[mesuresIndex].Count() / (float)note;
        tempsUntilNextNote = (60 / (float)tempo) / coeff;
    }
    void HandleNextNote() {
        if (!started)
            HandleFirstNote();
        else {
            if (mesures.Count > 0) {
                float tempsEcoule = Time.time - startingTime;
                //Debug.Log("tps ecoulé : " + tempsEcoule);
                if (tempsEcoule >= tempsUntilNextNote) {
                    if (mesures[mesuresIndex][notesIndex].isTrue)
                        PlaySound(mesures[mesuresIndex][notesIndex].integer);

                    float coeff = mesures[mesuresIndex].Count() / (float)note;
                    tempsUntilNextNote = 60 / (tempo * coeff);

                    //Debug.Log("coeff is : " + coeff + " and seconds to wait is : " + tempsUntilNextNote);
                    notesIndex = (notesIndex + 1) % mesures[mesuresIndex].Count();
                    if (notesIndex == 0) {

                        mesureCount++;
                        if (mesureCount % 4 == 0)
                            styleLead = style;
                        mesuresIndex = (mesuresIndex + 1) % mesures.Count;
                    }

                    startingTime = (tempsUntilNextNote - tempsEcoule) + Time.time;
                    //Debug.Log("temps ecoulé : " + tempsEcoule + " / startingTime : " + startingTime + " / timer start : " + (Time.time - startingTime));
                }
            }
        }
    }

    // Update is called once per frame
    void Update() {
        leadEvent.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject, myRigidbody));
        if (Time.timeSinceLevelLoad > 3)
            HandleNextNote();
    }

    float getNextPitch() {
        int rtn = 1;
        System.Random rnd = new System.Random();
        int rand = rnd.Next(0, 100);
        if (rand < 10) //revient a do4
        { rtn = 7; }
        else if (rand > 9 && rand < 20) //reste sur la meme note
        { rtn = currentIndex; }
        else if (rand > 19 && rand < 35) //descend de 1 note
        {
            if (currentIndex - 1 < 0) {
                rtn = currentIndex + 1;
            }
            else {
                rtn = currentIndex - 1;
            }
        }
        else if (rand > 34 && rand < 50) //monte de 1 note
        {
            if (currentIndex + 1 >= 15) {
                rtn = currentIndex - 1;
            }
            else {
                rtn = currentIndex + 1;
            }
        }
        else if (rand > 49 && rand < 60) //descend de 2 note
        {
            if (currentIndex - 2 < 0) {
                rtn = currentIndex + 2;
            }
            else {
                rtn = currentIndex - 2;
            }
        }
        else if (rand > 59 && rand < 70) //monte de 2 note
        {
            if (currentIndex + 2 >= 15) {
                rtn = currentIndex - 2;
            }
            else {
                rtn = currentIndex + 2;
            }
        }
        else if (rand > 69 && rand < 75) //descend de 3 note
        {
            if (currentIndex - 3 < 0) {
                rtn = currentIndex + 3;
            }
            else {
                rtn = currentIndex - 3;
            }
        }
        else if (rand > 74 && rand < 80) //monte de 3 note
        {
            if (currentIndex + 3 >= 15) {
                rtn = currentIndex - 3;
            }
            else {
                rtn = currentIndex + 3;
            }
        }
        else if (rand > 79 && rand < 84) //descend de 4 note
        {
            if (currentIndex - 4 < 0) {
                rtn = currentIndex + 4;
            }
            else {
                rtn = currentIndex - 4;
            }
        }
        else if (rand > 83 && rand < 88) //monte de 4 note
        {
            if (currentIndex + 4 >= 15) {
                rtn = currentIndex - 4;
            }
            else {
                rtn = currentIndex + 4;
            }
        }
        else if (rand > 87 && rand < 91) //descend de 5 note
        {
            if (currentIndex - 5 < 0) {
                rtn = currentIndex + 5;
            }
            else {
                rtn = currentIndex - 5;
            }
        }
        else if (rand > 90 && rand < 94) //monte de 5 note
        {
            if (currentIndex + 5 >= 15) {
                rtn = currentIndex - 5;
            }
            else {
                rtn = currentIndex + 5;
            }
        }
        else if (rand > 93 && rand < 96) //descend de 6 note
        {
            if (currentIndex - 6 < 0) {
                rtn = currentIndex + 6;
            }
            else {
                rtn = currentIndex - 6;
            }
        }
        else if (rand > 95 && rand < 98) //monte de 6 note
        {
            if (currentIndex + 6 >= 15) {
                rtn = currentIndex - 6;
            }
            else {
                rtn = currentIndex + 6;
            }
        }
        else if (rand > 97 && rand < 99) //descend de 7 note
        {
            if (currentIndex - 7 < 0) {
                rtn = currentIndex + 7;
            }
            else {
                rtn = currentIndex - 7;
            }
        }
        else //monte de 7 note
        {
            if (currentIndex + 7 >= 15) {
                rtn = currentIndex - 7;
            }
            else {
                rtn = currentIndex + 7;
            }
        }
        currentIndex = rtn;
        return pitches[rtn];
    }
}

