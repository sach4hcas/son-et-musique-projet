﻿using System;
using UnityEngine;

public class BrouillardBehaviour : MonoBehaviour
{
    protected float brouillard = 0;
    public void SetBrouillard(float value) {
        brouillard = value;
    }
    public float GetBrouillard() {
        return brouillard;
    }
}