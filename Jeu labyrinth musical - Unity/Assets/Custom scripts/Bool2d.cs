﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class BoolInt
{
    public int integer;
    public bool isTrue;

    public BoolInt() {
        integer = 40;
        isTrue = false;
    }
    public BoolInt(bool _b) {
        integer = 0;
        isTrue = _b;
    }

    public BoolInt(int _i, bool _b) {
        integer = _i;
        isTrue = _b;
    }
}

[System.Serializable]
public class BoolIntArray
{
    public List<BoolInt> row = new List<BoolInt>();
    public BoolIntArray() {

    }
    public BoolIntArray(int size) {
        for (int i = 0; i < size; i++)
            Add(new BoolInt());
    }

    public int Count() {
        return row.Count;
    }
    public BoolInt At(int index) {
        return row[index];
    }

    public void Set(int index, BoolInt value) {
        row[index] = value;
    }
    public BoolInt this[int index]
    {
        get {
            return At(index);
        }
        set {
            Set(index, value);
        }
    }
    public void Add(BoolInt boolInt) {
        row.Add(boolInt);
    }
    public void RemoveRange(int index, int count) {
        row.RemoveRange(index, count);
    }
}


[System.Serializable]
public class BoolArray
{
    public List<bool> row = new List<bool>();
    public BoolArray() {
        
    }
    public BoolArray(int size) {
        for (int i = 0; i < size; i++)
            Add(false);
    }

    public int Count() {
        return row.Count;
    }
    public bool At(int index) {
        return row[index];
    }

    public void Set(int index, bool value) {
        row[index] = value;
    }
    public bool this[int index]
    {
        get {
            return At(index);
        }
        set {
            Set(index, value);
        }
    }
    public void Add(bool boolean) {
        row.Add(boolean);
    }
    public void RemoveRange(int index, int count) {
        row.RemoveRange(index, count);
    }
}
