﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChordsScript : StyleBehaviour {

    [FMODUnity.EventRef]
    public string chords;
    FMOD.Studio.EventInstance chordsEvent;

    Rigidbody myRigidbody;

    bool started = false;

    // Use this for initialization
    void Start () {
        chordsEvent = FMODUnity.RuntimeManager.CreateInstance(chords);
        myRigidbody = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
        chordsEvent.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject, myRigidbody));
        chordsEvent.setParameterValue("Brouillard", brouillard);
        chordsEvent.setParameterValue("style", style);
        if (!started && Time.timeSinceLevelLoad > 3.1) {
            //Debug.Log("starting bass");
            started = true;
            chordsEvent.start();
        }
    }
}
