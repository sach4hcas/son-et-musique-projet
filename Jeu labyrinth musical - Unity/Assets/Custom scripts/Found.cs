﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Found : MonoBehaviour {

    public GameObject Character;
    public Winning WinningScript;

    private bool found = false;
    private Collider characterCollider;
	// Use this for initialization
	void Start () {
        characterCollider = Character.GetComponent<Collider>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider col) {
       // Debug.Log(col + " entered " + this);
        if (Collider.ReferenceEquals(col, characterCollider)) {
            if (!found) {
                Debug.Log("found batterie");
                found = true;
                WinningScript.ItemFound(this.gameObject);
            }

        }
    }
}
