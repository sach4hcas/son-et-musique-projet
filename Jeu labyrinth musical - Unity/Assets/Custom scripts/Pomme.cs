﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pomme : MonoBehaviour {

    [FMODUnity.EventRef]
    public string croque;
    FMOD.Studio.EventInstance croqueEvent;


    public StyleBehaviour bass;
    public StyleBehaviour chords;
    public StyleBehaviour lead;
    
    // Use this for initialization
    void Start () {
        croqueEvent = FMODUnity.RuntimeManager.CreateInstance(croque);
    }
	
    void OnTriggerEnter(Collider other) {
        Debug.Log("collision between " + this + " and " + other);
        if (other.tag.Equals("GameController"))
            EatMe();

    }

    public void EatMe() {
        Debug.Log("Pomme is gonna be eaten");
        bass.ChangeStyle();
        chords.ChangeStyle();
        lead.ChangeStyle();

        croqueEvent.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        croqueEvent.start();
        croqueEvent.release();

        gameObject.SetActive(false);
    }
	// Update is called once per frame
	void Update () {
		
	}
}
