﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBrouillard : MonoBehaviour
{


    public List<BrouillardBehaviour> soundScripts = new List<BrouillardBehaviour>();
    // Use this for initialization
    void Start() {

    }

    void OnTriggerStay(Collider other) {
        if (other.tag.Equals("Brouillard")) {
            float brouillardIntensity = other.GetComponentInParent<Brouillard>().intensity;
            for (int i = 0; i < soundScripts.Count; i++) {
                soundScripts[i].SetBrouillard(brouillardIntensity);
            }
            /*string debug = "[";
            for (int i = 0; i < soundScripts.Count; i++)
                debug += soundScripts[i].GetBrouillard() + ",";
            Debug.Log(debug + "]");*/

            

        }
    }

    void OnTriggerExit(Collider other) {
        if (other.tag.Equals("Brouillard")) {
            for (int i = 0; i < soundScripts.Count; i++)
                soundScripts[i].SetBrouillard(0);
            /*string debug = "[";
            for (int i = 0; i < soundScripts.Count; i++)
                debug += soundScripts[i].GetBrouillard() + ",";
            Debug.Log(debug + "]");*/
        }
        

    }
    // Update is called once per frame
    void Update() {

    }
}
