﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

[System.Serializable]
public enum Note
{
    doubleCroche = 16,
    croche = 8,
    noir = 4,
    blanche = 2,
    ronde = 1
}

public class HammerScript : MonoBehaviour
{
    [SerializeField]
    public int tempo = 130;
    [SerializeField]
    public int nbNote = 4;
    [SerializeField]
    public Note note = Note.noir;
    [SerializeField]
    public float maxDistance = 10000;

    //public Bool2D mesures = new Bool2D();
    [SerializeField]
    public List<BoolIntArray> mesures;

    public Transform HittingPoint;

    Rigidbody myRigidbody;
    bool launchable = true;
    float startingTime;
    float tempsUntilNextNote;

    int mesuresIndex = 0;
    int notesIndex = 0;

    bool started = false;



    // Use this for initialization
    void Start() {
        myRigidbody = GetComponent<Rigidbody>();

    }

    void Launch(float POWER) {
        if (launchable) {
            launchable = false;
            myRigidbody.useGravity = false;
            float speed = POWER / 2;
            //Debug.Log("speed = " + speed);
            PlaceHammer(speed, 0.1f);
            myRigidbody.velocity = -transform.up * speed;

            //myRigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;

        }
    }

    void OnCollisionEnter() {
        //Debug.Log("OnCollision");
        myRigidbody.useGravity = true;
        myRigidbody.constraints = RigidbodyConstraints.None;
        launchable = true;

    }

    void PlaceHammer(float speed, float secondUntilHit) {

        transform.rotation = HittingPoint.rotation;
        transform.position = HittingPoint.position;
        float minDistance = 0.1f;
        float distance = Mathf.Min(maxDistance, speed * secondUntilHit + minDistance);
        transform.Translate(Vector3.up * distance);
        //Debug.Log("Hammer placed");
    }

    void HandleFirstNote() {
        started = true;
        if (mesures[0][0].isTrue)
            Launch(mesures[0][0].integer);
        notesIndex = (notesIndex + 1) % mesures[mesuresIndex].Count();
        if (notesIndex == 0)
            mesuresIndex = (mesuresIndex + 1) % mesures.Count;
        startingTime = Time.time;
        float coeff = mesures[mesuresIndex].Count() / (float)note;
        tempsUntilNextNote = (60 / (float)tempo) / coeff;
    }
    void HandleNextNote() {
        if (!started)
            HandleFirstNote();
        else {
            if (mesures.Count > 0) {
                float tempsEcoule = Time.time - startingTime;
                //Debug.Log("tps ecoulé : " + tempsEcoule);
                if (tempsEcoule >= tempsUntilNextNote) {
                    if (mesures[mesuresIndex][notesIndex].isTrue)
                        Launch(mesures[mesuresIndex][notesIndex].integer);

                    float coeff = mesures[mesuresIndex].Count() / (float)note;
                    tempsUntilNextNote = 60 / (tempo * coeff);

                    //Debug.Log("coeff is : " + coeff + " and seconds to wait is : " + tempsUntilNextNote);
                    notesIndex = (notesIndex + 1) % mesures[mesuresIndex].Count();
                    if (notesIndex == 0)
                        mesuresIndex = (mesuresIndex + 1) % mesures.Count;

                    startingTime = (tempsUntilNextNote - tempsEcoule) + Time.time;
                    //Debug.Log("temps ecoulé : " + tempsEcoule + " / startingTime : " + startingTime + " / timer start : " + (Time.time - startingTime));
                }
            }
        }
    }

    // Update is called once per frame
    void Update() {
        if (Time.timeSinceLevelLoad>3)
            HandleNextNote();
    }
}

