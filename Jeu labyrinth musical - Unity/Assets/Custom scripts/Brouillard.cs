﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Brouillard : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string vent;
    FMOD.Studio.EventInstance ventEvent;
    [Range(0.0f, 1.0f)]
    public float intensity = 0;

    [Range(0.0f, 1.0f)]
    public float ventInitial = 0.5f;

    [Range(0.0f, .5f)]
    public float randomizerRange = 0.1f;


    // Use this for initialization
    void Start() {
        ventEvent = FMODUnity.RuntimeManager.CreateInstance(vent);
        ventEvent.setParameterValue("violence", ventInitial);
        ventEvent.start();
    }

    // Update is called once per frame
    void Update() {
        ventEvent.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
    }

    /*void OnTriggerEnter(Collider other) {

        if (other.tag.Equals("GameController")) {
            Debug.Log("entering brouillard");
            ventEvent.start();
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.tag.Equals("GameController")) {
            Debug.Log("exiting brouillard");
            ventEvent.release();
        }
    }*/

}
