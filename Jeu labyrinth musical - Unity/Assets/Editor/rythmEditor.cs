﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(HammerScript))]
public class rythmEditor : Editor
{

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        

        HammerScript hammer = (HammerScript)target;
        List<BoolIntArray> mesures = hammer.mesures;
        

        for (int i = 0; i < mesures.Count; i++) {
            GUILayout.BeginHorizontal();

            for (int j = 0; j < mesures[i].Count(); j++) {
                GUILayout.BeginVertical();
                mesures[i][j].isTrue = EditorGUILayout.Toggle(mesures[i][j].isTrue);
                mesures[i][j].integer = EditorGUILayout.IntField(mesures[i][j].integer,GUILayout.Width(20));
                GUILayout.EndVertical();
            }


            if (GUILayout.Button("-")) {
                mesures[i].RemoveRange(
                    (int)mesures[i].Count() / 2,
                    (int)mesures[i].Count() - mesures[i].Count() / 2);
            }
            if (GUILayout.Button("+")) {
                int mesureCount = mesures[i].Count();
                for (int j = mesureCount; j < mesureCount * 2; j++) {
                    mesures[i].Add(new BoolInt());
                    mesures[i][j].isTrue = EditorGUILayout.Toggle(mesures[i][j].isTrue);
                }
            }
            if (GUILayout.Button("X"))
                mesures.RemoveAt(i);

            GUILayout.EndHorizontal();

        }

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Add empty mesure")) {
            mesures.Add(new BoolIntArray());
            for (int i = 0; i < hammer.nbNote; i++)
                mesures[mesures.Count - 1].Add(new BoolInt());
            //SerializedProperty mesuresProp = serializedObject.FindProperty("mesures");
            //EditorGUILayout.PropertyField(mesuresProp, true);
            //serializedObject.ApplyModifiedProperties();
            
            //EditorUtility.SetDirty(hammer);
            //EditorSceneManager.MarkSceneDirty(hammer.gameObject.scene);
        }
        GUILayout.EndHorizontal();
        



    }
}